.. _ch-information:

Issues to be aware of for |RELEASENAME|
==========================================================================

Sometimes, changes introduced in a new release have side-effects we
cannot reasonably avoid, or they expose bugs somewhere else. This
section documents issues we are aware of. Please also read the errata,
the relevant packages' documentation, bug reports, and other information
mentioned in :ref:`morereading`.

.. _upgrade-specific-issues:

Upgrade specific items for |RELEASENAME|
----------------------------------------------------------------------------

This section covers items related to the upgrade from |OLDRELEASENAME| to
|RELEASENAME|.

.. _non-free-split:

Non-free firmware moved to its own component in the archive
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As described in :ref:`archive-areas`, non-free firmware packages are
now served from a dedicated archive component, called
``non-free-firmware``. To ensure installed non-free firmware packages
receive proper upgrades, changes to the APT configuration are required.
Assuming the ``non-free`` component was only added to the APT
sources-list to install firmware, the updated APT source-list entry
could look like:

.. code-block:: sources.list

   deb https://deb.debian.org/debian bookworm main non-free-firmware

If you were pointed to this chapter by ``apt`` you can prevent it from
continuously notifying you about this change by creating an
:url-man-stable:`apt.conf(5)` file named
``/etc/apt/apt.conf.d/no-bookworm-firmware.conf`` with the following
content:

.. parsed-literal::

   APT::Get::Update::SourceListWarnings::NonFreeFirmware "false";

.. _set-system-clock:

Changes to packages that set the system clock
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The **ntp** package, which used to be the default way to set the system
clock from a Network Time Protocol (NTP) server, has been replaced by
**ntpsec**.

Most users will not need to take any specific action to transition from
**ntp** to **ntpsec**.

In bookworm there are also several other packages that provide a similar
service. The Debian default is now **systemd-timesyncd**, which may be
adequate for users who only need an ``ntp`` client to set their clock.
bookworm also includes **chrony** and **openntpd** which support more advanced
features, such as operating your own NTP server.

.. _puppetserver:

Puppet configuration management system upgraded to 7
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Puppet has been upgraded from 5 to 7, skipping the Puppet 6 series
altogether. This introduces major changes to the Puppet ecosystem.

The classic Ruby-based Puppet Master 5.5.x application has been
deprecated upstream and is no longer available in Debian. It is replaced
by Puppet Server 7.x, provided by the **puppetserver** package. The package
is automatically installed as a dependency of the transitional
**puppet-master** package.

In some cases, Puppet Server is a drop-in replacement for Puppet Master,
but you should review the configuration files available under
``/etc/puppet/puppetserver`` to ensure the new defaults are suitable for
your deployment. In particular the legacy format for the ``auth.conf``
file is deprecated, see the
`auth.conf documentation <https://www.puppet.com/docs/puppet/7/server/config_file_auth.html>`__
for details.

The recommended approach is to upgrade the server before clients. The
Puppet 7 Server is
`backwards compatible with older clients <https://www.puppet.com/docs/puppet/7/server/compatibility_with_puppet_agent.html>`__;
a Puppet 5 Server can still handle upgraded agents but cannot register
new Puppet 7 agents. So if you deploy new Puppet 7 agents before
upgrading the server, you will not be able to add them to the fleet.

The **puppet** package has been replaced by the **puppet-agent** package and is
now a transitional package to ensure a smooth upgrade.

Finally, the **puppetdb** package was removed in bullseye but is
reintroduced in bookworm.

.. _youtube-dl:

youtube-dl replaced with yt-dlp
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The popular tool **youtube-dl**, which can download videos from a large
variety of websites (including, but not limited to, YouTube) is no
longer included in Debian. Instead, it has been replaced with an empty
transitional package that pulls in the **yt-dlp** package instead. **yt-dlp** is
a fork of **youtube-dl** where new development is currently happening.

There are no compatibility wrappers provided, so you'll need to modify
your scripts and personal behavior to call ``yt-dlp`` instead of
``youtube-dl``. The functionality should be mostly the same, although
some options and behavioral details have changed. Be sure to check
:url-man-stable:`yt-dlp.1`
for details, and in particular
:url-man-stable:`yt-dlp.1.html#Differences_in_default_behavior`.

.. _fcitx-fcitx5-bookworm-conflict:

Fcitx versions no longer co-installable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The packages **fcitx** and **fcitx5** provide version 4 and version 5 of the
popular Fcitx Input Method Framework. Following upstream's
recommendation, they can no longer be co-installed on the same operating
system. Users should determine which version of Fcitx is to be kept if
they had co-installed **fcitx** and **fcitx5** previously.

Before the upgrade, users are strongly encouraged to purge all related
packages for the unwanted Fcitx version (``fcitx-*`` for Fcitx 4, and
``fcitx5-*`` for Fcitx 5). When the upgrade is finished, consider
executing the ``im-config`` again to select the desired input method
framework to be used in the system.

You can read more background information in
`the announcement posted on the mailing list <https://lists.debian.org/debian-chinese-gb/2021/12/msg00000.html>`__
(text written in Simplified Chinese).

.. _mariadb-versionless-package-names:

MariaDB package names no longer include version numbers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Unlike bullseye that had the MariaDB version in package names (e.g.
**mariadb-server-10.5** and **mariadb-client-10.5**), in bookworm the equivalent
MariaDB 10.11 package names are fully versionless (e.g. **mariadb-server**
or **mariadb-client**). The MariaDB version is still visible in the package
version metadata.

There is at least one known upgrade scenario (`Bug
#1035949 <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1035949>`_) where the transition to versionless
package names fails: running

.. code-block:: console

   # apt-get install default-mysql-server

may fail when **mariadb-client-10.5** and the file
``/usr/bin/mariadb-admin`` in it is removed before the MariaDB server
SysV init service has issued a shutdown, which uses ``mariadb-admin``.
The workaround is to run

.. code-block:: console

   # apt upgrade

before running

.. code-block:: console

   # apt full-upgrade

.

For more information about the package name changes in MariaDB, see
`/usr/share/doc/mariadb-server/NEWS.Debian.gz <https://salsa.debian.org/mariadb-team/mariadb-server/-/blob/280369f034bedae714af2f26111d1d9d1659f142/debian/mariadb-server.NEWS>`__.

.. _system-logging:

Changes to system logging
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The **rsyslog** package is no longer needed on most systems and you may be
able to remove it.

Many programs produce log messages to inform the user of what they are
doing. These messages can be managed by systemd's "journal" or by a
"syslog daemon" such as ``rsyslog``.

In |OLDRELEASENAME|, **rsyslog** was installed by default and the systemd
journal was configured to forward log messages to rsyslog, which writes
messages into various text files such as ``/var/log/syslog``.

From |RELEASENAME|, **rsyslog** is no longer installed by default. If you do
not want to continue using ``rsyslog``, after the upgrade you can mark
it as automatically installed with

.. code-block:: console

   # apt-mark auto rsyslog

and then an

.. code-block:: console

   # apt autoremove

will remove it, if possible. If you have upgraded from older Debian
releases, and not accepted the default configuration settings, the
journal may not have been configured to save messages to persistent
storage: instructions for enabling this are in
:url-man-stable:`journald.conf(5)`.

If you decide to switch away from **rsyslog** you can use the ``journalctl``
command to read log messages, which are stored in a binary format under
``/var/log/journal``. For example,

.. code-block:: console

   # journalctl -e

shows the most recent log messages in the journal and

.. code-block:: console

   # journalctl -ef

shows new messages as they are written (similar to running

.. code-block:: console

   # tail -f /var/log/syslog

).

.. _rsyslog-timestamp-change-affects-logcheck:

rsyslog changes affecting log analyzers such as logcheck
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**rsyslog** now defaults to "high precision timestamps" which may affect
other programs that analyze the system logs. There is further
information about how to customize this setting in
:url-man-stable:`rsyslog.conf(5)`.

The change in timestamps may require locally-created **logcheck** rules to
be updated. ``logcheck`` checks messages in the system log (produced by
``systemd-journald`` or ``rsyslog``) against a customizable database of
regular expressions known as rules. Rules that match the time the
message was produced will need to be updated to match the new
``rsyslog`` format. The default rules, which are provided by the
**logcheck-database** package, have been updated, but other rules, including
those created locally, may require updating to recognize the new format.
See
`/usr/share/doc/logcheck-database/NEWS.Debian.gz <https://salsa.debian.org/debian/logcheck/-/blob/debian/1.4.0/debian/logcheck-database.NEWS>`__
for a script to help update local ``logcheck`` rules.

.. _rsyslog-creates-fewer-logfiles:

rsyslog creates fewer log files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**rsyslog** has changed which log files it creates, and some files in
``/var/log`` can be deleted.

If you are continuing to use **rsyslog** (see `Changes to system
logging <#changes-to-system-logging>`__), some log files in ``/var/log``
will no longer be created by default. The messages that were written to
these files are also in ``/var/log/syslog`` but are no longer created by
default. Everything that used to be written to these files will still be
available in ``/var/log/syslog``.

The files that are no longer created are:

-  ``/var/log/mail.{info,warn,err}``

   These files contained messages from the local mail transport agent
   (MTA), split up by priority.

   As ``/var/log/mail.log`` contains all mail related messages, these
   files (and their rotated counterparts) can be deleted safely. If you
   were using those files to monitor anomalies, a suitable alternative
   might be something like logcheck.

-  ``/var/log/lpr.log``

   This file contained log messages relating to printing. The default
   print system in debian is **cups** which does not use this file, so
   unless you installed a different printing system this file (and its
   rotated counterparts) can be deleted.

-  ``/var/log/{messages,debug,daemon.log}``

   These files (and their rotated counterparts) can be deleted.
   Everything that used to be written to these files will still be in
   ``/var/log/syslog``.

.. _openldap-2.5:

slapd upgrade may require manual intervention
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

OpenLDAP 2.5 is a major new release and includes several incompatible
changes as described in `the upstream release
announcement <https://git.openldap.org/openldap/openldap/-/raw/OPENLDAP_REL_ENG_2_5/ANNOUNCEMENT>`__.
Depending on the configuration, the ``slapd`` service might remain
stopped after the upgrade, until necessary configuration updates are
completed.

The following are some of the known incompatible changes:

-  The :url-man-oldstable:`slapd-bdb(5)` and
   :url-man-oldstable:`slapd-hdb(5)` database
   backends have been removed. If you are using one of these backends
   under |OLDRELEASENAME|, it is strongly recommended to migrate to the
   :url-man-stable:`slapd-mdb(5)`
   backend *before* upgrading to |RELEASENAME|.

-  The :url-man-oldstable:`slapd-shell(5)`
   database backend has been removed.

-  The :url-man-stable:`slapo-ppolicy(5)`
   overlay now includes its schema compiled into the module. The old
   external schema, if present, conflicts with the new built-in one.

-  The contrib password module pw-argon2
   (see :url-man-oldstable:`slapd-pw-argon2.5`)
   has been renamed to
   :url-man-stable:`argon2`.

Instructions for completing the upgrade and resuming the ``slapd``
service can be found in
`/usr/share/doc/slapd/README.Debian.gz <https://sources.debian.org/src/openldap/bookworm/debian/slapd.README.Debian/>`__.
You should also consult `the upstream upgrade
notes <https://openldap.org/doc/admin25/appendix-upgrading.html>`__.

.. _grub-os-prober:

GRUB no longer runs os-prober by default
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For a long time, **grub** has used the **os-prober** package to detect other
operating systems installed on a computer so that it can add them to the
boot menu. Unfortunately, that can be problematic in certain cases (e.g.
where guest virtual machines are running), so this has now been disabled
by default in the latest upstream release.

If you are using GRUB to boot your system and want to continue to have
other operating systems listed on the boot menu, you can change this.
Either edit the file ``/etc/default/grub``, ensure you have the setting
``GRUB_DISABLE_OS_PROBER=false`` and re-run ``update-grub``, or run

.. code-block:: console

   # dpkg-reconfigure <GRUB_PACKAGE>

to change this and other GRUB settings in a more user-friendly way.

GNOME has reduced accessibility support for screen readers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Many ``GNOME`` apps have switched from the ``GTK3`` graphics toolkit to
``GTK4``. Sadly, this has made many apps much less usable with screen
readers such as **orca**.

If you depend on a screen reader you should consider switching to a
different desktop such as `Mate <https://mate-desktop.org>`__, which has
better accessibility support. You can do this by installing the
**mate-desktop-environment** package. Information about how to use Orca
under Mate is available `here <https://wiki.debian.org/Accessibility/Orca#MATE>`__.

.. _i386-is-i686:

Baseline for 32-bit PC is now i686
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Debian's support for 32-bit PC (known as the Debian architecture
i386) now no longer covers any i586 processor. The new minimum
requirement is i686. This means that the i386 architecture
now requires the "long NOP" (NOPL) instruction, while bullseye still
supported some i586 processors without that instruction (e.g. the "AMD
Geode").

If your machine is not compatible with this requirement, it is
recommended that you stay with bullseye for the remainder of its support
cycle.

.. _polkit-configuration:

Changes to polkit configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For consistency with upstream and other distributions, the ``polkit``
(formerly ``PolicyKit``) service, which allows unprivileged programs to
access privileged system services, has changed the syntax and location
for local policy rules. You should now write local rules for customizing
the security policy in
`JavaScript <https://wikipedia.org/wiki/JavaScript>`__, and place them
at ``/etc/polkit-1/rules.d/*.rules``. Example rules using the new format
can be found in ``/usr/share/doc/polkitd/examples/``, and
:url-man-unstable:`polkit.8.html#AUTHORIZATION_RULES` has further information.

Previously, rules could be written in ``pkla`` format, and placed in
subdirectories of ``/etc/polkit-1/localauthority`` or
``/var/lib/polkit-1/localauthority``. However, ``.pkla`` files should
now be considered deprecated, and will only continue to work if the
**polkitd-pkla** package is installed. This package will usually be
installed automatically when you upgrade to bookworm, but it is likely
not to be included in future Debian releases, so any local policy
overrides will need to be migrated to the JavaScript format.

.. _merged-usr-required:

A "merged-``/usr``" is now required
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Debian has adopted a filesystem layout, referred to as
"merged-``/usr``", which no longer includes the legacy directories
``/bin``, ``/sbin``, ``/lib``, or optional variants such as ``/lib64``.
In the new layout, the legacy directories are replaced with symlinks to
the corresponding locations ``/usr/bin``, ``/usr/sbin``, ``/usr/lib``,
and ``/usr/lib64``. This means that, for example, both ``/bin/bash`` and
``/usr/bin/bash`` will launch ``bash``.

For systems installed as buster or bullseye there will be no change, as
the new filesystem layout was already the default in these releases.
However, the older layout is no longer supported, and systems using it
will be converted to the new layout when they are upgraded to bookworm.

The conversion to the new layout should have no impact on most users.
All files are automatically moved to their new locations even if they
were installed locally or come from packages not provided by Debian, and
hardcoded paths such as ``/bin/sh`` continue to work. There are,
however, some potential issues:

-  .. code-block:: console

      # dpkg --search

   will give wrong answers for files moved to the new locations:

   .. code-block:: console

      # dpkg --search /usr/bin/bash

   will not identify that ``bash`` came from a package. (But

   .. code-block:: console

      # dpkg --search /bin/bash

   still works as expected.)

-  Local software not provided by Debian may not support the new layout
   and may, for example, rely on ``/usr/bin/name`` and ``/bin/name``
   being two different files. This is not supported on merged systems
   (including new installations since buster), so any such software must
   be fixed or removed before the upgrade.

-  Systems that rely on a "base layer" that is not directly writable
   (such as
   `WSL1 <https://wiki.debian.org/InstallingDebianOn/Microsoft/Windows/SubsystemForLinux>`__
   images or container systems using multi-layer
   `overlayfs <https://en.wikipedia.org/wiki/OverlayFS>`__ filesystems)
   cannot be safely converted and should either be replaced (e.g., by
   installing a new WSL1 image from the store) or have each individual
   layer upgraded (e.g., by upgrading the base Debian layer of the
   overlayfs independently) rather than dist-upgraded.

For further information, see `The Case for the /usr
merge <https://www.freedesktop.org/wiki/Software/systemd/TheCaseForTheUsrMerge/>`__
and `the Debian Technical Committee resolution <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=994388#110>`__.

.. _libcrypt-upgrade-from-buster:

Unsupported upgrades from buster fail on ``libcrypt1``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Debian officially supports upgrades only from one stable release to the
next, e.g. from bullseye to bookworm. Upgrades from buster to bookworm
are not supported, and will fail due to `Bug
#993755 <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=993755>`__ with the following error:

.. parsed-literal::

   Setting up libc6:ARCH (2.36-9) ...
   /usr/bin/perl: error while loading shared libraries: libcrypt.so.1: cannot open shared object file: No such file or directory
   dpkg: error processing package libc6:ARCH (--configure):
   installed libc6:ARCH package post-installation script subprocess returned error exit status 127
           

It is however possible to manually recover from this particular
situation by forcibly installing the new libcrypt1:

.. code-block:: console

   # cd $(mktemp -d)
   # apt download libcrypt1
   # dpkg-deb -x libcrypt1_*.deb .
   # cp -ra lib/* /lib/
   # apt --fix-broken install
           

.. _openssh-pam-environment-removed:

openssh-server no longer reads ~/.pam_environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Secure Shell (SSH) daemon provided in the **openssh-server** package,
which allows logins from remote systems, no longer reads the user's
``~/.pam_environment`` file by default; this feature has a `history of
security problems <https://bugs.debian.org/1030119>`__ and has been
deprecated in current versions of the Pluggable Authentication Modules (PAM)
library.  If you used this feature, you should switch from setting variables
in ``~/.pam_environment`` to setting them in your shell initialization files
(e.g. ``~/.bash_profile`` or ``~/.bashrc``) or some other similar mechanism
instead.

Existing SSH connections will not be affected, but new connections may
behave differently after the upgrade.  If you are upgrading remotely, it is
normally a good idea to ensure that you have some other way to log into the
system before starting the upgrade; see :ref:`recovery`.


.. _openssh-dsa-removal:

OpenSSH no longer supports DSA keys
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Digital Signature Algorithm (DSA) keys, as specified in the Secure Shell
(SSH) protocol, are inherently weak: they are limited to 160-bit private
keys and the SHA-1 digest.  The SSH implementation provided by the
**openssh-client** and **openssh-server** packages has disabled support for
DSA keys by default since OpenSSH 7.0p1 in 2015, released with Debian 9
("stretch"), although it could still be enabled using the
``HostKeyAlgorithms`` and ``PubkeyAcceptedAlgorithms`` configuration options
for host and user keys respectively.

The only remaining uses of DSA at this point should be connecting to some
very old devices.  For all other purposes, the other key types supported by
OpenSSH (RSA, ECDSA, and Ed25519) are superior.

As of OpenSSH 9.8p1 in trixie, DSA keys are no longer supported even with
the above configuration options.  If you have a device that you can only
connect to using DSA, then you can use the ``ssh1`` command provided by the
**openssh-client-ssh1** package to do so.

In the unlikely event that you are still using DSA keys to connect to a
Debian server (if you are unsure, you can check by adding the ``-v`` option
to the ``ssh`` command line you use to connect to that server and looking
for the "Server accepts key:" line), then you must generate replacement keys
before upgrading.  For example, to generate a new Ed25519 key and enable
logins to a server using it, run this on the client, replacing
``username@server`` with the appropriate user and host names:

.. code-block:: console

   $ ssh-keygen -t ed25519
   $ ssh-copy-id username@server


.. _before-first-reboot:

Things to do post upgrade before rebooting
--------------------------------------------------------------------------------

When ``apt full-upgrade`` has finished, the "formal" upgrade is
complete. For the upgrade to |RELEASENAME|, there are no special actions
needed before performing a reboot.

.. only:: fixme

	When ``apt full-upgrade`` has finished, the "formal" upgrade is
	complete, but there are some other things that should be taken care of
	*before* the next reboot.

	::

	   add list of items here
       
         

.. _not-upgrade-only:

Items not limited to the upgrade process
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _limited-security-support:

Limitations in security support
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are some packages where Debian cannot promise to provide minimal
backports for security issues. These are covered in the following
subsections.

.. note::

   The package **debian-security-support** helps to track the security
   support status of installed packages.

.. _browser-security:

Security status of web browsers and their rendering engines
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Debian |RELEASE| includes several browser engines which are affected by a
steady stream of security vulnerabilities. The high rate of
vulnerabilities and partial lack of upstream support in the form of long
term branches make it very difficult to support these browsers and
engines with backported security fixes. Additionally, library
interdependencies make it extremely difficult to update to newer
upstream releases. Applications using the **webkit2gtk** source package
(e.g. **epiphany**) are covered by security support, but applications using
qtwebkit (source package **qtwebkit-opensource-src**) are not.

For general web browser use we recommend Firefox or Chromium. They will
be kept up-to-date by rebuilding the current ESR releases for stable.
The same strategy will be applied for Thunderbird.

Once a release becomes ``oldstable``, officially supported browsers may
not continue to receive updates for the standard period of coverage. For
example, Chromium will only receive 6 months of security support in
``oldstable`` rather than the typical 12 months.

.. _golang-static-linking:

Go- and Rust-based packages
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Debian infrastructure currently has problems with rebuilding
packages of types that systematically use static linking. With the
growth of the Go and Rust ecosystems it means that these packages will
be covered by limited security support until the infrastructure is
improved to deal with them maintainably.

In most cases if updates are warranted for Go or Rust development
libraries, they will only be released via regular point releases.

.. _python3-pep-668:

Python Interpreters marked externally-managed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Debian provided python3 interpreter packages (**python3.11** and **pypy3**)
are now marked as being externally-managed, following
`PEP-668 <https://peps.python.org/pep-0668/>`__. The version of
**python3-pip** provided in Debian follows this, and will refuse to manually
install packages on Debian's python interpreters, unless the
``--break-system-packages`` option is specified.

If you need to install a Python application (or version) that isn't
packaged in Debian, we recommend that you install it with ``pipx`` (in
the **pipx** Debian package). ``pipx`` will set up an environment isolated
from other applications and system Python modules, and install the
application and its dependencies into that.

If you need to install a Python library module (or version) that isn't
packaged in Debian, we recommend installing it into a virtualenv, where
possible. You can create virtualenvs with the ``venv`` Python stdlib
module (in the **python3-venv** Debian package) or the ``virtualenv`` Python
3rd-party tool (in the **virtualenv** Debian package). For example, instead
of running
``pip install --user foo``, run: ``mkdir -p ~/.venvs && python3 -m venv ~/.venvs/foo && ~/.venvs/foo/bin/python -m pip install foo``
to install it in a dedicated virtualenv.

See ``/usr/share/doc/python3.11/README.venv`` for more details.

.. _vlc-ffmpeg-5:

Limited hardware-accelerated video encoding/decoding support in VLC
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The VLC video player supports hardware-accelerated video decoding and
encoding via VA-API and VDPAU. However, VLC's support for VA-API is
tightly related to the version of FFmpeg. Because FFmpeg was upgraded to
the 5.x branch, VLC's VA-API support has been disabled. Users of GPUs
with native VA-API support (e.g., Intel and AMD GPUs) may experience
high CPU usage during video playback and encoding.

Users of GPUs offering native VDPAU support (e.g., NVIDIA with non-free
drivers) are not affected by this issue.

Support for VA-API and VDPAU can be checked with ``vainfo`` and
``vdpauinfo`` (each provided in a Debian package of the same name).

.. _systemd-resolved:

systemd-resolved has been split into a separate package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The new **systemd-resolved** package will not be installed automatically on
upgrades. If you were using the ``systemd-resolved`` system service,
please install the new package manually after the upgrade, and note that
until it has been installed, DNS resolution might no longer work since
the service will not be present on the system. Installing this package
will automatically give systemd-resolved control of
``/etc/resolv.conf``. For more information about systemd-resolved,
consult the official
`documentation <https://www.freedesktop.org/software/systemd/man/systemd-resolved.service.html>`__.
Note that systemd-resolved was not, and still is not, the default DNS
resolver in Debian. If you have not configured your machine to use
systemd-resolved as the DNS resolver, no action is required.

.. _systemd-boot:

systemd-boot has been split into a separate package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The new **systemd-boot** package will not be installed automatically on
upgrades. If you were using ``systemd-boot``, please install this new
package manually, and note that until you do so, the older version of
systemd-boot will be used as the bootloader. Installing this package
will automatically configure systemd-boot as the machine's bootloader.
The default boot loader in Debian is still GRUB. If you have not
configured the machine to use systemd-boot as the bootloader, no action
is required.

.. _systemd-journal-remote:

systemd-journal-remote no longer uses GnuTLS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The optional
`systemd-journal-gatewayd <https://www.freedesktop.org/software/systemd/man/systemd-journal-remote.service.html#--trust=>`__
and
`systemd-journal-remote <https://www.freedesktop.org/software/systemd/man/systemd-journal-gatewayd.service.html#--trust=>`__
services are now built without GnuTLS support, which means the
``--trust`` option is no longer provided by either program, and an error
will be raised if it is specified.

.. _adduser-changes:

Extensive changes in adduser for bookworm
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There have been several changes in **adduser**. The most prominent change is
that ``--disabled-password`` and ``--disabled-login`` are now
functionally identical. For further details, please read the
``/usr/share/doc/adduser/NEWS.Debian.gz``.

.. _xen-network:

Predictable naming for Xen network interfaces
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The predictable naming logic in systemd for network interfaces has been
extended to generate stable names from Xen netfront device information.
This means that instead of the former system of names assigned by the
kernel, interfaces now have stable names of the form ``enX#``. Please
adapt your system before rebooting after the upgrade. Some more
information can be found on the `NetworkInterfaceNames wiki
page <https://wiki.debian.org/NetworkInterfaceNames#bookworm-xen>`__.

.. _dash-circumflex:

Change in dash handling of circumflex
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``dash``, which by default provides the system shell ``/bin/sh`` in
Debian, has switched to treating the circumflex (``^``) as a literal
character, as was always the intended POSIX-compliant behavior. This
means that in bookworm ``[^0-9]`` no longer means "not 0 to 9" but "0 to
9 and ^".

.. _netcat-openbsd-now-supports-abstract-sockets:

netcat-openbsd supports abstract sockets
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The ``netcat`` utility for reading and writing data across network
connections supports abstract sockets
(see :url-man-stable:`unix.7.html#Abstract_sockets`,
and uses them by default in some circumstances.

By default, ``netcat`` is provided by **netcat-traditional**. However, if
``netcat`` is provided by the **netcat-openbsd** package and you are using
an ``AF_UNIX`` socket, then this new default applies. In this case the
``-U`` option to ``nc`` will now interpret an argument starting with an
``@`` as requesting an abstract socket rather than as a filename
beginning with an ``@`` in the current directory. This can have security
implications because filesystem permissions can no longer be used to
control access to an abstract socket. You can continue to use a filename
starting with an ``@`` by prefixing the name with ``./`` or by
specifying an absolute path.

.. _obsolescense-and-deprecation:

Obsolescence and deprecation
--------------------------------------------------------

.. _noteworthy-obsolete-packages:

Noteworthy obsolete packages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following is a list of known and noteworthy obsolete packages (see
:ref:`obsolete` for a description).

The list of obsolete packages includes:

-  The **libnss-ldap** package has been removed from |RELEASENAME|. Its
   functionalities are now covered by **libnss-ldapd** and **libnss-sss**.

-  The **libpam-ldap** package has been removed from |RELEASENAME|. Its
   replacement is **libpam-ldapd**.

-  The **fdflush** package has been removed from |RELEASENAME|. In its stead,
   please use ``blockdev --flushbufs`` from **util-linux**.

-  The **libgdal-perl** package has been removed from |RELEASENAME|, because
   the Perl binding for GDAL is no longer supported upstream. If you
   need Perl support for GDAL, you can migrate to the FFI interface
   provided by the **Geo::GDAL::FFI** package, available on CPAN. You will
   have to build your own binaries as documented on the
   `BookwormGdalPerl Wiki page <https://wiki.debian.org/BookwormGdalPerl>`__.

.. _deprecated-components:

Deprecated components for |RELEASENAME|
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With the next release of Debian |NEXTRELEASE| (codenamed |NEXTRELEASENAME|)
some features will be deprecated. Users will need to migrate to other
alternatives to prevent trouble when updating to Debian |NEXTRELEASE|.

This includes the following features:

-  Development of the NSS service ``gw_name`` stopped in 2015. The
   associated package **libnss-gw-name** may be removed in future Debian
   releases. The upstream developer suggests using **libnss-myhostname**
   instead.

-  **dmraid** has not seen upstream activity since end 2010 and has been on
   life support in Debian. bookworm will be the last release to ship it,
   so please plan accordingly if you're using **dmraid**.

-  **request-tracker4** has been superseded by **request-tracker5** in this
   release, and will be removed in future releases. We recommend that
   you plan to migrate from **request-tracker4** to **request-tracker5** during
   the lifetime of this release.

-  The **isc-dhcp** suite has been
   `deprecated <https://www.isc.org/blogs/isc-dhcp-eol/>`__ by the
   `ISC <https://www.isc.org/>`__. The `Debian Wiki <https://wiki.debian.org/>`__ has
   a list of alternative implementations, see the `DHCP
   Client <https://wiki.debian.org/DHCP_Client>`__ and `DHCP
   Server <https://wiki.debian.org/DHCP_Server>`__ pages for the latest. If you are
   using NetworkManager or systemd-networkd, you can safely remove the
   **isc-dhcp-client** package as they both ship their own implementation.
   If you are using the **ifupdown** package, you can experiment with udhcpc
   as a replacement. The ISC recommends the **Kea** package as a replacement
   for DHCP servers.

   The security team will support the **isc-dhcp** package during the
   bookworm lifetime, but the package will likely be unsupported in the
   next stable release, see `bug #1035972 (isc-dhcp
   EOL'ed) <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1035972>`__ for more details.

.. only:: fixme

   No-longer-supported hardware
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   For a number of \`arch`-based devices that were supported in
   |OLDRELEASENAME|, it is no longer viable for Debian to build the required
   ``Linux`` kernel, due to hardware limitations. The unsupported devices
   are:

   -  foo

   Users of these platforms who wish to upgrade to |RELEASENAME| nevertheless
   should keep the |OLDRELEASENAME| APT sources enabled. Before upgrading
   they should add an APT preferences file containing:

   .. parsed-literal::

      Package: linux-image-marvell
      Pin: release n= |OLDRELEASENAME|
      Pin-Priority: 900

   The security support for this configuration will only last until
   |OLDRELEASENAME|'s End Of Life.

.. _rc-bugs:

Known severe bugs
---------------------------------------------------

Although Debian releases when it's ready, that unfortunately doesn't
mean there are no known bugs. As part of the release process all the
bugs of severity serious or higher are actively tracked by the Release
Team, so an `overview of those
bugs <https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=release.debian.org@packages.debian.org;tag=&releasename;-can-defer>`__
that were tagged to be ignored in the last part of releasing |RELEASENAME|
can be found in the `Debian Bug Tracking System <https://bugs.debian.org/>`__. The
following bugs were affecting |RELEASENAME| at the time of the release and
worth mentioning in this document:

+----------------------+---------------------------+------------------------------+
| Bug number           | Package (source or        | Description                  |
|                      | binary)                   |                              |
+======================+===========================+==============================+
| `1032240`_           | **akonadi-backend-mysql** | akonadi server fails         |
|                      |                           | to start since it            |
|                      |                           | cannot connect to            |
|                      |                           | mysql database               |
+----------------------+---------------------------+------------------------------+
| `1032177`_           | **faketime**              | faketime doesn't             |
|                      |                           | fake time (on i386)          |
+----------------------+---------------------------+------------------------------+
| `918984`_            | **src:fuse3**             | provide upgrade path         |
|                      |                           | fuse -> fuse3 for            |
|                      |                           | bookworm                     |
+----------------------+---------------------------+------------------------------+
| `1016903`_           | **g++-12**                | tree-vectorize:              |
|                      |                           | Wrong code at O2             |
|                      |                           | level                        |
|                      |                           | (-fno-tree-vectorize         |
|                      |                           | is working)                  |
+----------------------+---------------------------+------------------------------+
| `1020284`_           | **git-daemon-run**        | fails to purge:              |
|                      |                           | deluser -f: Unknown          |
|                      |                           | option: f                    |
+----------------------+---------------------------+------------------------------+
| `919296`_            | **git-daemon-run**        | fails with 'warning:         |
|                      |                           | git-daemon: unable           |
|                      |                           | to open                      |
|                      |                           | supervise/ok: file           |
|                      |                           | does not exist'              |
+----------------------+---------------------------+------------------------------+
| `1034752`_           | **src:gluegen2**          | embeds non-free headers      |
+----------------------+---------------------------+------------------------------+

.. _1032240: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1032240
.. _1032177: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1032177
.. _918984: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=918984
.. _1016903: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1016903
.. _1020284: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1020284
.. _919296: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=919296
.. _1034752: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1034752

# Malayalam translation of installing.po
# Copyright (C) 2009 Free Software Foundation, Inc.
# This file is distributed under the same license as the release-notes
# package.
#
# Praveen P <pravin.vet@gmail.com>, 2009.
# SANKARANARAYANAN |ശങ്കരനാരായണന്‍ <snalledam@dataone.in>, 2009.
# Praveen Arimbrathodiyil <pravi.a@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: lenny release notes\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-10-06 00:00+0200\n"
"PO-Revision-Date: 2009-02-09 14:43-0800\n"
"Last-Translator: Praveen Arimbrathodiyil <pravi.a@gmail.com>\n"
"Language: ml\n"
"Language-Team: Malayalam <smc-discuss@googlegroups.com>\n"
"Plural-Forms: nplurals=2; plural=n != 1\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../installing.rst:4
msgid "Installation System"
msgstr "ഇന്‍സ്റ്റാളേഷന്‍ ഉപാധി"

#: ../installing.rst:6
#, fuzzy
msgid ""
"The Debian Installer is the official installation system for Debian. It "
"offers a variety of installation methods. The methods that are available "
"to install your system depend on its architecture."
msgstr ""
"ഡെബിയന്‍ ഇന്‍സ്റ്റാളര്‍ ഡെബിയന്റെ ഔദ്യോഗിക ഇന്‍സ്റ്റാളേഷന്‍ ഉപാധിയാണ്. "
"അത് വിവിധ ഇന്‍സ്റ്റാളേഷന്‍ രീതികള്‍ പ്രദാനം ചെയ്യുന്നു. അവയില്‍ "
"നിങ്ങള്‍ക്ക് വേണ്ടത് നിങ്ങളുടെ സിസ്റ്റത്തിന്റെ വാസ്തുവിദ്യയെ "
"ആശ്രയിച്ചിരിയ്ക്കുന്നു."

#: ../installing.rst:10
#, fuzzy
msgid ""
"Images of the installer for |RELEASENAME| can be found together with the "
"Installation Guide on the Debian website (|URL-INSTALLER|)."
msgstr ""
"|RELEASENAME| നായി ഉള്ള ഇന്‍സ്റ്റാളറിന്റെ ഇമേജുകള്‍ <ulink url=\"&url-"
"installer;\">Debian website</ulink> ലെ ഇന്‍സ്റ്റാളേഷന്‍ സഹായിയോടൊപ്പം "
"കണ്ടെത്താം."

# | msgid ""
# | "The Installation Guide is also included on the first CD/DVD of the "
# | "official Debian CD/DVD sets, at:"
#: ../installing.rst:13
#, fuzzy
msgid ""
"The Installation Guide is also included on the first media of the "
"official Debian DVD (CD/blu-ray) sets, at:"
msgstr ""
"ഇന്‍സ്റ്റാളേഷന്‍ സഹായി ഔദ്യോഗിക ഡെബിയന്‍ സിഡി/ഡിവിഡി സെറ്റിലെ ഒന്നം "
"സിഡി/ഡീവിഡി യില്‍ ഉള്‍പ്പെടുത്തിയിരിയ്ക്കുന്നത്:"

#: ../installing.rst:20
#, fuzzy
msgid ""
"You may also want to check the errata for debian-installer at |URL-"
"INSTALLER-ERRATA| for a list of known issues."
msgstr ""
"<ulink url=\"&url-installer;index#errata\">errata</ulink> ല്‍ പരിശോധിച്ച്"
" ഡെബിയന്‍-ഇന്‍സ്റ്റാളറിന്റെ തിരിച്ചറിഞ്ഞിട്ടുള്ള പ്രശ്നങ്ങളുടെ ഒരു പട്ടിക"
" കണ്ടിരിയ്ക്കേണ്ടതാണ്."

#: ../installing.rst:26
msgid "What's new in the installation system?"
msgstr "ഇന്‍സ്റ്റാളേഷന്‍ ഉപാധിയില്‍ എന്താണ് പുതുതായി ഉള്ളത്?"

# | msgid ""
# | "There has been a lot of development on the Debian Installer since its "
# | "first official release with Debian 3.1 (sarge)  resulting in both "
# | "improved hardware support and some exciting new features."
#: ../installing.rst:28
#, fuzzy
msgid ""
"There has been a lot of development on the Debian Installer since its "
"previous official release with Debian |OLDRELEASE|, resulting in improved"
" hardware support and some exciting new features or improvements."
msgstr ""
"ഡെബിയന്‍ ഇന്‍സ്റ്റാളറിന്  അത് ആദ്യമായി  Debian 3.1 (സാര്‍ജ്) നോടൊപ്പം "
"പ്രകാശിതമായതില്‍ പിന്നെ മെച്ചപ്പെട്ട ഹാര്‍ഡ്വെയര്‍ പിന്തുണയുടെയും "
"മറ്റനവധി പുത്തന്‍ സൌകര്യങ്ങളുടെയും രൂപത്തില്‍ ധാരാളം പുരോഗതികള്‍ "
"ഉണ്ടായിട്ടുണ്ട്."

#: ../installing.rst:32
msgid ""
"If you are interested in an overview of the changes since "
"|OLDRELEASENAME|, please check the release announcements for the "
"|RELEASENAME| beta and RC releases available from the Debian Installer's "
"`news history <https://www.debian.org/devel/debian-installer/News/>`__."
msgstr ""

#: ../installing.rst:41
msgid "Something"
msgstr ""

#: ../installing.rst:43
msgid "Text"
msgstr ""

# | msgid "Automated installation"
#: ../installing.rst:48
#, fuzzy
msgid "Cloud installations"
msgstr "സ്വയംനിയന്ത്രിത ഇന്‍സ്റ്റാളേഷന്‍"

#: ../installing.rst:50
msgid ""
"The `cloud team <https://wiki.debian.org/Teams/Cloud>`__ publishes Debian"
" |RELEASENAME| for several popular cloud computing services including:"
msgstr ""

#: ../installing.rst:53
msgid "Amazon Web Services"
msgstr ""

#: ../installing.rst:55
msgid "Microsoft Azure"
msgstr ""

#: ../installing.rst:57
msgid "OpenStack"
msgstr ""

#: ../installing.rst:59
msgid "Plain VM"
msgstr ""

#: ../installing.rst:61
msgid ""
"Cloud images provide automation hooks via ``cloud-init`` and prioritize "
"fast instance startup using specifically optimized kernel packages and "
"grub configurations. Images supporting different architectures are "
"provided where appropriate and the cloud team endeavors to support all "
"features offered by the cloud service."
msgstr ""

#: ../installing.rst:67
msgid ""
"The cloud team will provide updated images until the end of the LTS "
"period for |RELEASENAME|. New images are typically released for each "
"point release and after security fixes for critical packages. The cloud "
"team's full support policy can be found `here "
"<https://wiki.debian.org/Cloud/ImageLifecycle>`__."
msgstr ""

#: ../installing.rst:73
msgid ""
"More details are available at `<https://cloud.debian.org/>`__ and `on the"
" wiki <https://wiki.debian.org/Cloud/>`__."
msgstr ""

#: ../installing.rst:79
msgid "Container and Virtual Machine images"
msgstr ""

#: ../installing.rst:81
msgid ""
"Multi-architecture Debian |RELEASENAME| container images are available on"
" `Docker Hub <https://hub.docker.com/_/debian>`__. In addition to the "
"standard images, a \"slim\" variant is available that reduces disk usage."
msgstr ""

#: ../installing.rst:86
msgid ""
"Virtual machine images for the Hashicorp Vagrant VM manager are published"
" to `Vagrant Cloud <https://app.vagrantup.com/debian>`__."
msgstr ""

#~ msgid "Major changes"
#~ msgstr "പ്രധാന മാറ്റങ്ങള്‍"

#~ msgid "Removed ports"
#~ msgstr "പുതിയ പോര്‍ട്ടുകള്‍"

#~ msgid "New ports"
#~ msgstr "പുതിയ പോര്‍ട്ടുകള്‍"

#~ msgid "New languages"
#~ msgstr "പുതിയ ഭാഷകള്‍"

#~ msgid ""
#~ "The languages that can only be "
#~ "selected using the graphical installer "
#~ "as their character sets cannot be "
#~ "presented in a non-graphical environment"
#~ " are: Amharic, Bengali, Dzongkha, Gujarati,"
#~ " Hindi, Georgian, Kannada, Khmer, "
#~ "Malayalam, Marathi, Nepali, Punjabi, Tamil,"
#~ " Telugu, Tibetan and Uyghur."
#~ msgstr ""
#~ "അമാരിക്ക്, ബംഗാളി, സോങ്ക, ഗുജറാത്തി, ഹിന്ദി,"
#~ " ജോര്‍ജിയന്‍, ഖ്മെര്‍, മലയാളം, മറാഠി, "
#~ "നേപ്പാളി, പഞ്ചാബി, തമിഴ്, തായ് എന്നിവയുടെ "
#~ "അക്ഷരസഞ്ചയങ്ങളെ ചിത്രാധിഷ്ഠിതമല്ലാത്ത അന്തരീക്ഷത്തില്‍ "
#~ "അവതരിപ്പിയ്ക്കാന്‍ കഴിയാത്തതിനാല്‍ ചിത്രാധിഷ്ഠിതമായ "
#~ "ഇന്‍സ്റ്റാളറില്‍ മാത്രം തിരഞ്ഞെടുക്കാനാവുന്ന "
#~ "ഭാഷകളാണ്. "

#~ msgid "Help during the installation process"
#~ msgstr "ഇന്‍സ്റ്റാളേഷന്‍ ഉപാധിയില്‍ എന്താണ് പുതുതായി ഉള്ളത്?"

#~ msgid "Support for loading firmware debs during installation"
#~ msgstr "ഇന്‍സ്റ്റാളേഷന്‍ സമയത്ത് ഫേംവേര്‍ ലോഡ് ചെയ്യാനുള്ള സൌകര്യം"

#~ msgid "Live system installation"
#~ msgstr "സ്വയംനിയന്ത്രിത ഇന്‍സ്റ്റാളേഷന്‍"

#~ msgid ""
#~ "It is now possible to load "
#~ "firmware binary files from removable "
#~ "media when they're provided externally "
#~ "to Debian installation media."
#~ msgstr ""
#~ "ഫേംവേറിന്റെ ബൈനറി ഫയലുകള്‍ ഒരു "
#~ "തിരിച്ചെടുക്കനാകുന്ന മാധ്യമത്തിലാക്കിക്കൊടുത്താല്‍ അതും"
#~ " ലോഡ് ചെയ്യാന്‍ ഡെബിയന്‍ ഇന്‍സ്റ്റാളറിന് "
#~ "ഇപ്പോള്‍ കഴിവുണ്ട്."

#~ msgid ""
#~ "Support for installation from Microsoft "
#~ "Windows <indexterm><primary>Microsoft "
#~ "Windows</primary></indexterm>"
#~ msgstr ""
#~ "മൈക്രോസോഫ്റ്റ് വിന്‍ഡോസില്‍ <indexterm><primary>Microsoft"
#~ " Windows</primary></indexterm> ഇന്‍സ്റ്റോള്‍ "
#~ "ചെയ്യുന്നതിനുള്ള കഴിവ് "

#~ msgid ""
#~ "The installation media are now provided"
#~ " with an application that allows "
#~ "preparing the system to install Debian"
#~ " from Microsoft Windows environments."
#~ msgstr ""
#~ "ഇന്‍സ്റ്റാളേഷന്‍ മാധ്യമത്തില്‍ ഇപ്പോള്‍ "
#~ "മൈക്രോസോഫ്റ്റ് വിന്‍ഡോസ് അന്തരീക്ഷത്തില്‍ നിന്ന് "
#~ "കൊണ്ട് തന്നെ ഡെബിയന്‍ ഇന്‍സ്റ്റോള്‍ ചെയ്യാന്‍"
#~ " സിസ്റ്റത്തെ സജ്ജമാക്കുന്ന ഒരു പ്രയോഗം കൂടി"
#~ " ഉള്‍പ്പെടുത്തിയിട്ടുണ്ട്. "

#~ msgid "<acronym>SATA</acronym> <acronym>RAID</acronym> support"
#~ msgstr "<acronym>SATA</acronym> <acronym>RAID</acronym> പിന്തുണ"

#~ msgid "Early upgrade of packages with security fixes"
#~ msgstr "സുരക്ഷാ തിരുത്തലുകള്‍ വേണ്ട പാക്കേജുകളുടെ വേഗത്തിലുള്ള അപ്ഗ്രേഡ് "

#~ msgid ""
#~ "When used with functional network "
#~ "access, the installer will upgrade all"
#~ " packages that have been updated "
#~ "since the initial release of "
#~ "|RELEASENAME|. This upgrade happens during "
#~ "the installation step, before the "
#~ "installed system is booted."
#~ msgstr ""
#~ "ഒരു പ്രവര്‍ത്തനയോഗ്യമായ ശൃംഖലാ ബന്ധം "
#~ "ലഭ്യമാണെങ്കില്‍, |RELEASENAME| ന്റെ ആദ്യ "
#~ "പ്രകാശനം മുതല്‍ അപ്ഡേറ്റ് ചെയ്തിട്ടുള്ള എല്ലാ"
#~ " പാക്കേജുകളേയും ഇന്‍സ്റ്റാളര്‍ അപ്ഗ്രേഡ് ചെയ്യും."
#~ " ഈ അപ്ഗ്രേഡ്, ഇന്‍സ്റ്റോള്‍ ചെയ്യപ്പെട്ട "
#~ "സിസ്റ്റം ബൂട്ട് ചെയ്യുന്നതിന് മുന്‍പ് തന്നെ"
#~ " നടക്കുകയും ചെയ്യുന്നു."

#~ msgid ""
#~ "As a consequence, the installed system"
#~ " is less likely to be vulnerable "
#~ "to security issues that were discovered"
#~ " and fixed between the release time"
#~ " of |RELEASENAME| and the installation "
#~ "time."
#~ msgstr ""
#~ "തത്ഫലമായി, ഇന്‍സ്റ്റോള്‍ ചെയ്യപ്പെട്ട സിസ്റ്റം "
#~ "|RELEASENAME|ന്റെ പ്രകാശനത്തിനു ശേഷം "
#~ "ഇന്‍സ്റ്റാളേഷന്‍ സമയത്തിനുള്ളില്‍ തിരിച്ചറിഞ്ഞിട്ടുള്ളതും"
#~ " പരിഹരിച്ചിട്ടുള്ളവയുമായ സുരക്ഷാത്തകരാറുകളാല്‍ "
#~ "ബാധിയ്ക്കപ്പെടാനുള്ള സാധ്യത വളരെക്കുറവാണ്."

#~ msgid "Support for <emphasis>volatile</emphasis>"
#~ msgstr "<emphasis>volatile</emphasis> നുള്ള പിന്തുണ"

#~ msgid ""
#~ "The installer can now optionally set "
#~ "up the installed system to use "
#~ "updated packages from "
#~ "<literal>volatile.debian.org</literal>. This archive "
#~ "hosts packages providing data that needs"
#~ " to be regularly updated over time,"
#~ " such as timezones definitions, anti-"
#~ "virus signature files, etc."
#~ msgstr ""
#~ "ഇന്‍സ്റ്റാളറിന് ഇപ്പോള്‍ ഇന്‍സ്റ്റോള്‍ ചെയ്യപ്പെട്ട"
#~ " സിസ്റ്റത്തിനെ <literal>volatile.debian.org</literal> "
#~ "ല്‍ നിന്നും ലഭ്യമായ അപ്ഡേറ്റു ചെയ്യപ്പെട്ട "
#~ "പാക്കേജുകള്‍ ഉപയോഗിക്കത്തക്കവണ്ണം സജ്ജീകരിയ്ക്കാനാകും. "
#~ "ആ ശേഖരം  സമയകാല നിര്‍വ്വചനങ്ങള്‍, ആന്റി-വൈറസ്"
#~ " ഒപ്പുകള്‍ തുടങ്ങിയ സമയാനുഗതമായി പുതുക്കേണ്ട "
#~ "വിവരങ്ങള്‍ നല്കുന്ന  പാക്കേജുകള്‍ക്ക് "
#~ "ആഥിത്യമരുളുന്നു."

#~ msgid "New boot menu for Intel x86 and AMD64"
#~ msgstr "ഇന്റല്‍ x86 AMD64 എന്നിവയ്ക്ക് പുതിയ ബൂട്ട് മെനു"

#~ msgid ""
#~ "An interactive boot menu was added "
#~ "to make the choice of specific "
#~ "options and boot methods more intuitive"
#~ " for users."
#~ msgstr ""
#~ "പ്രത്യേക ഐച്ഛികങ്ങളും ബൂട്ട് രീതികളും "
#~ "തിരഞ്ഞെടിക്കല്‍ ഉപയോക്താവിന് കൂടുതല്‍ "
#~ "സൌകര്യപ്രദമാക്കുവാന്‍ ഉപയോക്താവിനോട് സംവദിയ്ക്കുന്ന "
#~ "ഒരു ബൂട്ട് മെനു ചേര്‍ത്തിരിയ്ക്കുന്നു."

#~ msgid ""
#~ "The armel architecture is now supported."
#~ " Images for i386 Xen guests are "
#~ "also provided."
#~ msgstr ""
#~ "ആമെല്‍ വസ്തുവിദ്യയെ ഇപ്പോള്‍ പിന്തുണയ്ക്കുന്നു. "
#~ "i386 Xen അഥിതികള്‍ക്ക് വേണ്ട ഇമേജുകള്‍ "
#~ "ഇപ്പോള്‍ ലഭ്യമാണ്."

#~ msgid "Support for hardware speech synthesis devices"
#~ msgstr "ഹാര്‍ഡ്വേര്‍ സംഭാഷണോത്പാദകോപകരണങ്ങള്‍ക്കുള്ള പിന്തുണ"

#~ msgid ""
#~ "Several devices designed to provide "
#~ "hardware speech synthesis are now "
#~ "supported by the installer, therefore "
#~ "improving its accessibility for visually-"
#~ "impaired users.  <indexterm><primary>visually-"
#~ "impaired users</primary></indexterm>"
#~ msgstr ""
#~ "ഹാര്‍ഡ്‌വെയര്‍ സംഭാഷണോത്പാദനത്തിനുതകുന്ന ധാരാളം "
#~ "ഉപകരണങ്ങളെ ഇപ്പോള്‍ ഇന്‍സ്റ്റാളര്‍ "
#~ "പിന്തുണയ്ക്കുന്നതിനാല്‍, കാഴ്ചക്കുറവുള്ള "
#~ "ഉപയോക്ത്താക്കള്‍ക്കും അതിന്റെ ഉപയോഗ്യത "
#~ "വര്‍ദ്ധിച്ചിട്ടുണ്ട്. <indexterm><primary>visually-"
#~ "impaired users</primary></indexterm>"

#~ msgid "Support for <literal>relatime</literal> mount options"
#~ msgstr "<literal>relatime</literal> മൌണ്ട് ഓപ്ഷനുകള്‍ക്കുള്ള പിന്തുണ"

#~ msgid ""
#~ "The installer can now set up "
#~ "partitions with the <literal>relatime</literal> "
#~ "mount option, so that access time "
#~ "on files and directories is updated "
#~ "only if the previous access time "
#~ "was earlier than the current modify "
#~ "or change time."
#~ msgstr ""
#~ "ഇന്‍സ്റ്റാളറിന് ഇപ്പോള്‍ <literal>relatime</literal> "
#~ "മൌണ്ട് ഓപ്ഷനോടു കൂടിയ വിഭജനങ്ങളെയും "
#~ "ക്രമീകരിയ്ക്കാനാവും. ഇത്  ഫയലുകള്‍ക്കും "
#~ "ഡയറക്ടറികള്‍ക്കും മാറ്റം വരുത്തിയ സമയം "
#~ "പരിഷ്കരിയ്ക്കന്‍ സഹായിയ്കുന്നു."

#~ msgid "NTP clock synchronization at installation time"
#~ msgstr "ഇന്‍സ്റ്റാളേഷന്‍ സമയത്ത് NTP ഘടികാരം ക്രമീകരിയ്ക്കുന്നു"

#~ msgid ""
#~ "The computer clock is now synchronized"
#~ " with NTP servers over the network"
#~ " during installation so that the "
#~ "installed system immediately has an "
#~ "accurate clock."
#~ msgstr ""
#~ "കമ്പ്യൂട്ടറുന്റെ ഘടികാരം ഇപ്പോള്‍ ഇന്‍സ്റ്റാളേഷന്‍"
#~ " സമയത്ത് തന്നെ എന്‍റ്റിപി സെര്‍വറുകളുടെ "
#~ "സമയത്തിനൊപ്പം ശൃഖലയിലൂടെ ക്രമീകരിയ്ക്കപ്പെടുന്നതിനാല്‍ "
#~ "ഇന്‍സ്റ്റോള്‍ ചെയ്യപ്പെട്ട സിസ്റ്റം അപ്പോള്‍ "
#~ "തന്നെ കൃത്യമായ സമയം കാണിയ്ക്കുന്നു."

#~ msgid ""
#~ "Thanks to the huge efforts of "
#~ "translators, Debian can now be installed"
#~ " in 63 languages (50 using the "
#~ "text-based installation user interface and"
#~ " 13 supported only with the graphical"
#~ " user interface).  This is five "
#~ "languages more than in |OLDRELEASENAME|.  "
#~ "Languages added in this release include"
#~ " Amharic, Marathi, Irish, Northern Sami,"
#~ " and Serbian.  Due to lack of "
#~ "translation updates, one language has "
#~ "been dropped in this release: Estonian."
#~ " Another language that was disabled "
#~ "in |OLDRELEASENAME| has been reactivated: "
#~ "Welsh."
#~ msgstr ""
#~ "വിവര്‍ത്തകരുടെ വലിയ പ്രയത്നങ്ങള്‍ക്കു നന്ദി'ഡെബിയന്‍"
#~ " ഇപ്പോള്‍ 63 ഭാഷകളില്‍ ഇന്‍സ്റ്റോള്‍ "
#~ "ചെയ്യാനാകും(50 എണ്ണം അക്ഷരാധിഷ്ഠിത ഇന്‍സ്റ്റാളേഷനും"
#~ " 13 ചിത്രാധിഷ്ഠിത ഇന്‍സ്റ്റാളേഷനും). ഇത്  "
#~ "|OLDRELEASENAME| നേക്കാള്‍ 5 ഭാഷകള്‍ "
#~ "കൂടുതലാണ്. അമാരിക്, മറാഠി, ഐറിഷ്, വടക്കന്‍ "
#~ "സാമി, സെര്‍ബിയന്‍ എന്നിവയാണ് പുതിയ ഭാഷകള്‍."
#~ " വിവര്‍ത്തന അപ്ഡേറ്റുകളുടെ അഭാവത്താല്‍ "
#~ "എസ്റ്റോണിയന്‍ ഭാഷ ഈ റിലീസില്‍ "
#~ "ഉള്‍പ്പെടുത്തുന്നില്ല. |OLDRELEASENAME| ല്‍ "
#~ "ഉള്‍പ്പെടുത്താതിരുന്ന വെല്‍ഷ് ഭാഷ വീണ്ടും "
#~ "ഉള്‍പ്പെടുത്തുകയും ചെയ്തിരിയ്ക്കുന്നു."

#~ msgid "Simplified country choice"
#~ msgstr "ലഘുകരിച്ച രാജ്യം തിരഞ്ഞെടുക്കല്‍ പ്രക്രിയ"

#~ msgid ""
#~ "The country choice list is now "
#~ "grouped by continents, allowing an "
#~ "easier selection of country, when users"
#~ " don't want to pick the ones "
#~ "associated with the chosen language."
#~ msgstr ""
#~ "രാജ്യം തിരഞ്ഞെടുക്കല്‍ ഇപ്പോള്‍ ഭൂഖണ്ടക്രമത്തില്‍"
#~ " തരം തിരിച്ചിരിയ്ക്കുന്നതിനാല്‍ ഉപയോക്താക്കള്‍ക്ക് "
#~ "തിരഞ്ഞെടുത്ത ഭാഷയുമായി ബന്ധമില്ലാത്ത ഒരു "
#~ "രാജ്യമാണ് തിരഞ്ഞെടുക്കേണ്ടതെങ്കില്‍ കൂടി, അത് "
#~ "എളുപ്പം സാധിയ്ക്കുന്നു."

#~ msgid "Install Debian with a Braille display"
#~ msgstr "ഒരു ബ്രെയില്‍ പ്രദര്‍ശിനിയുപയോഗിച്ച് Debian ഇന്‍സ്റ്റോള്‍ ചെയ്യുക"

#~ msgid ""
#~ "You can install Debian |RELEASE| "
#~ "(|RELEASENAME|) with a Braille display. "
#~ "The Braille display must be connected"
#~ " to a <acronym>USB</acronym> or a "
#~ "serial port. If your Braille display "
#~ "is connected to a <acronym>USB</acronym> "
#~ "port and the American Braille table "
#~ "is supposed to be used, you can"
#~ " press <keycap>Enter</keycap> in the boot"
#~ " menu. The screen reader BrlTTY is"
#~ " standardized to search for Braille "
#~ "displays at the <acronym>USB</acronym> port"
#~ " and uses the American Braille table."
#~ " If the Braille display is connected"
#~ " to a serial port or if you "
#~ "want to use a different Braille "
#~ "table, you have to press the "
#~ "<keycap>Tab</keycap> key in the boot "
#~ "menu first. Then, you can configure "
#~ "the screen reader BrlTTY with the "
#~ "kernel parameter brltty."
#~ msgstr ""
#~ "നിങ്ങള്‍ക്ക് Debian |RELEASE| (|RELEASENAME|) "
#~ "ഒരു ബ്രെയില്‍ പ്രദര്‍ശിനി ഉപയോഗിച്ചു് "
#~ "ഇന്‍സ്റ്റോള്‍ ചെയ്യാനാകും. ബ്രെയില്‍ പ്രദര്‍ശിനി "
#~ "ഒരു <acronym>USB</acronym> യോടൊ ഒരു സീരിയല്‍"
#~ " പോര്‍ട്ടിനോടോ ഘടിപ്പിച്ചിരിയ്ക്കണം. ബ്രെയില്‍ "
#~ "പ്രദര്‍ശിനി <acronym>USB</acronym> പോര്‍ട്ടില്‍ "
#~ "ഘടിപ്പിച്ചിരിയ്ക്കുകയും നിങ്ങള്‍ക്ക് ഒരു "
#~ "അമേരിയ്ക്കന്‍ ബ്രെയില്‍ പട്ടിക ഉപയോഗിയ്ക്കുകയും "
#~ "വേണമെങ്കില്‍,ബൂട്ട് മെനുവില്‍ <keycap>Enter</keycap> "
#~ "അമര്‍ത്താവുന്നതാണ്. BrlTTY പ്രദര്‍ശിനി മാപിനി "
#~ "അമേരിയ്ക്കന്‍ ബ്രയില്‍പട്ടിക ഉപയോഗിക്കാനും "
#~ "<acronym>USB</acronym> പോര്‍ട്ടില്‍ ബ്രെയില്‍ "
#~ "പ്രദര്‍ശിനികള്‍ തെരയാനും തരപ്പെടുത്തിയിരിയ്ക്കുന്നു. "
#~ "ബ്രെയില്‍ പ്രദര്‍ശിനി ഒരു സീരിയല്‍ "
#~ "പോര്‍ട്ടിലാണ് ഘടിപ്പിച്ചിരിയ്ക്കുന്നതെങ്കിലോ നിങ്ങള്‍ക്ക്"
#~ " മറ്റൊരു ബ്രെയില്‍ പട്ടികയാണ് "
#~ "ഉപയൊഗിയ്ക്കെണ്ടതെങ്കിലോ ആദ്യം ബൂട്ട് മെനുവില്‍ "
#~ "<keycap>Tab</keycap> അമര്‍ത്തുക. അതിനു ശേഷം "
#~ "നിങ്ങള്‍ക്ക് BrlTTY പ്രദര്‍ശിനി മാപിനിയെ "
#~ "brltty കെര്‍ണല്‍ മാനദണ്ഡങ്ങള്‍ക്കൊപ്പം "
#~ "ക്രമീകരിയ്ക്കാം."

#~ msgid "For the parameter, the following syntax is valid:"
#~ msgstr "ഈ മാനദണ്ഡത്തിന്, താഴെക്കൊടുത്തിരിയ്ക്കുന്ന സിന്റാക്സ് സാധുവാണ്:"

#~ msgid "brltty=<replaceable>driver</replaceable>,<replaceable>device</replaceable>,<replaceable>table</replaceable>"
#~ msgstr "brltty=<replaceable>driver</replaceable>,<replaceable>device</replaceable>,<replaceable>table</replaceable>"

#~ msgid "All operands are optional."
#~ msgstr "എല്ലാ ഓപ്പറണ്ടുകളും ഐച്ഛികമാണ്."

#~ msgid "<varname>driver</varname>"
#~ msgstr "<varname>driver</varname>"

#~ msgid ""
#~ "The driver for the employed Braille "
#~ "display. Here, you must enter either "
#~ "a code consisting of two letters "
#~ "or the word <literal>auto</literal>. If "
#~ "this argument is not entered, the "
#~ "automated recognition is activated by "
#~ "default."
#~ msgstr ""
#~ "ഉപയോഗിയ്ക്കുന്ന ബ്രെയില്‍ പ്രദര്‍ശിനിയുടെ ഡ്രൈവര്‍."
#~ " ഇവിടെ, നിങ്ങള്‍ രണ്ടക്ഷരങ്ങളുള്ള ഒരു കോഡോ"
#~ " അല്ലെങ്കില്‍ <literal>auto</literal> എന്ന വാക്കോ"
#~ " പ്രവേശിപ്പിയ്ക്കുക. ഇതു ചെയ്തില്ലെങ്കില്‍, "
#~ "സ്വതവേയുള്ള ഒരു സ്വയംനിയന്ത്രിത സംവിധാനം "
#~ "പ്രവര്‍ത്തിപ്പിയ്ക്കപ്പെടും."

#~ msgid "<varname>device</varname>"
#~ msgstr "<varname>device</varname>"

#~ msgid ""
#~ "The device can be entered as "
#~ "relative to /dev/ as well as an"
#~ " absolute specification."
#~ msgstr ""
#~ "/dev/ നെ അപേക്ഷിച്ചോ ഒരു മുഴുവന്‍ "
#~ "പേരിലൂടെയോ ഉപകരണത്തെ സൂചിപ്പിയ്ക്കാവുന്നതാണു്."

#~ msgid "<varname>table</varname>"
#~ msgstr "<varname>table</varname>"

#~ msgid ""
#~ "Defines the Braille table for the "
#~ "desired language. By default, the US "
#~ "table is employed."
#~ msgstr ""
#~ "ആവശ്യമുള്ള ഭാഷയ്ക്ക് വേണ്ട ബ്രെയില്‍ പട്ടിക"
#~ " നിര്‍വചിയ്ക്കുന്നു. സ്വതവേ ക്രമീകരിച്ചിരിയ്ക്കുന്നത്"
#~ " അമേരിയ്ക്കന്‍ പട്ടികയാണ്."

#~ msgid "Examples"
#~ msgstr "ഉദാഹരണങ്ങള്‍"

#~ msgid "brltty=ht,ttyS0,de"
#~ msgstr "brltty=ht,ttyS0,de"

#~ msgid ""
#~ "The Handy Tech Driver is used. The"
#~ " Braille display is connected to "
#~ "Com1.  The German Braille table is "
#~ "used."
#~ msgstr ""
#~ "Handy Tech Driver ഉപയോഗിയ്ക്കുന്നു. ബ്രെയില്‍"
#~ " പ്രദര്‍ശിനി Com1 ല്‍ ഘടിപ്പിച്ചിരിയ്ക്കുന്നു."
#~ " ജര്‍മന്‍ ബ്രെയില്‍ പട്ടികയാണ് "
#~ "ഉപയോഗിച്ചുകൊണ്ടിരിയ്ക്കുന്നത്."

#~ msgid "brltty=,,de"
#~ msgstr "brltty=,,de"

#~ msgid ""
#~ "Here, only the German Braille table "
#~ "is specified. Therefore, BrlTTY will try"
#~ " to find a Braille display at a"
#~ " <acronym>USB</acronym> port."
#~ msgstr ""
#~ "ഇവിടെ, ജര്‍മന്‍ ബ്രെയില്‍ പട്ടിക മാത്രമേ "
#~ "തന്നിട്ടുള്ളൂ. അതിനാല്‍ BrlTTY, "
#~ "<acronym>USB</acronym> പോര്‍ട്ടില്‍ മറ്റൊരു "
#~ "ബ്രെയില്‍ പ്രദര്‍ശിനി കണ്ടെത്താന്‍ ശ്രമിയ്ക്കും."

#~ msgid "Install Debian with a hardware speech synthesis"
#~ msgstr ""
#~ "ഒരു ഹാര്‍ഡ്‌വെയര്‍ ശബ്ദ സിന്തസിസ് "
#~ "ഉപകരണമുപയോഗിച്ച് Debian ഇന്‍സ്റ്റോള്‍ ചെയ്യുക"

#~ msgid ""
#~ "Support for hardware speech synthesis is"
#~ " only available in the textual "
#~ "version of the installer. For size "
#~ "reasons, however, it is enabled along"
#~ " with support for the graphical "
#~ "installer, which needs more space "
#~ "anyway. You thus need to select "
#~ "the <computeroutput>Graphical install</computeroutput> "
#~ "entry in the boot menu."
#~ msgstr ""
#~ "ഹാര്‍ഡ്‌വെയര്‍ ശബ്ദ സിന്തസിസ് ഉപകരണത്തിനുള്ള "
#~ "പിന്തുണ പദാവലി മാത്രമുപയോഗിയ്ക്കുന്ന "
#~ "ഇന്‍സ്റ്റോളറില്‍ മാത്രമേ ലഭ്യമുള്ളൂ. എന്നിരുന്നാലും,"
#~ " വലിപ്പവുമായി ബന്ധപ്പെട്ട കാരണങ്ങളാല്‍, നേരത്തെ"
#~ " തന്നെ കൂടുതല്‍ വലിപ്പമുള്ള, ചിത്രാധിഷ്ഠിതമായ "
#~ "ഇന്‍സ്റ്റോളറിന്റെ പിന്തുണയോടു് കൂടി മാത്രമേ "
#~ "ഇതു് പ്രവര്‍ത്തന സജ്ജമാകുകയുള്ളൂ. അതു് കൊണ്ടു്"
#~ " തന്നെ ബൂട്ട് മെനുവില്‍ നിന്നും "
#~ "<computeroutput>Graphical install</computeroutput> "
#~ "തെരഞ്ഞെടുക്കേണ്ടതുണ്ടു്"

#~ msgid ""
#~ "Hardware speech synthesis can not be "
#~ "automatically detected.  You thus need "
#~ "to append the "
#~ "<userinput>speakup.synth=driver</userinput> boot parameter"
#~ " to tell Speakup which driver it "
#~ "should use. <userinput>driver</userinput> should "
#~ "be replaced by the driver code for"
#~ " your device, see <ulink url=\"&url-"
#~ "speakup-driver-codes;\"></ulink> for a "
#~ "list.  The textual version of the "
#~ "installer will then be automatically "
#~ "selected, and support for the speech "
#~ "synthesis will be automatically installed "
#~ "on the target system."
#~ msgstr ""
#~ "ഹാര്‍ഡ്‌വെയര്‍ ശബ്ദ സിന്തസിസ് ഉപകരണങ്ങള്‍ "
#~ "സ്വയം കണ്ടുപിടിയ്ക്കാന്‍ സാധ്യമല്ല. അതു് "
#~ "കൊണ്ടു് തന്നെ <userinput>speakup.synth=driver</userinput>"
#~ " എന്ന ബൂട്ട് പരാമീറ്റര്‍ അവസാനം ചേര്‍ത്തു്"
#~ " ഏതു് പ്രവര്‍ത്തകമാണു് ഉപയോഗിയ്ക്കേണ്ടതെന്നു് "
#~ "പറയേണ്ടതുണ്ടു്<userinput>driver</userinput> എന്നതിനു് പകരം"
#~ " നിങ്ങളുടെ ഉപകരണത്തിനുള്ള പ്രവര്‍ത്തക കോഡ് "
#~ "നല്‍കണം, അവയുടെ പട്ടികയ്ക്കു് <ulink url"
#~ "=\"&url-speakup-driver-codes;\"></ulink> കാണുക."
#~ " ഇന്‍സ്റ്റോളറിന്റെ പദാവലി പതിപ്പു് സ്വയം "
#~ "തെരഞ്ഞെടുക്കുന്നതും ലക്ഷ്യ സിസ്റ്റത്തില്‍ ശബ്ദ "
#~ "സിന്തസിസിനുള്ള പിന്തുണ സ്വയം ഇന്‍സ്റ്റോള്‍ "
#~ "ചെയ്യുന്നതുമായിരിയ്ക്കും."

#~ msgid "en"
#~ msgstr "ml"

#~ msgid "/doc/install/manual/<replaceable>language</replaceable>/index.html\n"
#~ msgstr "/doc/install/manual/<replaceable>language</replaceable>/index.html\n"

# | msgid ""
# | "In these Release Notes we'll only list the major changes in the "
# | "installer.  If you are interested in an overview of the detailed changes
# "
# | "since |OLDRELEASENAME|, please check the release announcements for the "
# | "|RELEASENAME| beta and RC releases available from the Debian Installer's
# "
# | "<ulink url=\"&url-installer-news;\">news history</ulink>."
#~ msgid ""
#~ "If you are interested in an "
#~ "overview of the detailed changes since"
#~ " |OLDRELEASENAME|, please check the release"
#~ " announcements for the |RELEASENAME| beta"
#~ " and RC releases available from the"
#~ " Debian Installer's <ulink url=\"&url-"
#~ "installer-news;\">news history</ulink>."
#~ msgstr ""
#~ "ഈ പ്രകാശനക്കുറിപ്പുകളില്‍ കുറച്ചു പ്രധാന "
#~ "പുരോഗതികള്‍ മാത്രമേ "
#~ "നിരത്തിയിട്ടുള്ളൂ.|OLDRELEASENAME|പ്രകാശന ശേഷമുണ്ടായ "
#~ "മാറ്റങ്ങളേ പറ്റി കൂടുതലറിയാന്‍ ഡെബിയന്‍ "
#~ "ഇന്‍സ്റ്റാളറിന്റെ <ulink url=\"&url-installer-"
#~ "news;\">news history</ulink> ല്‍ ലഭ്യമായ "
#~ "|RELEASENAME| ബീറ്റ , ആര്‍സി പ്രകാശനങ്ങളുടെ"
#~ " പ്രകാശന പ്രഖ്യാപനങ്ങള്‍ പരിശോധിയ്ക്കുക."

#~ msgid "Automated installation"
#~ msgstr "സ്വയംനിയന്ത്രിത ഇന്‍സ്റ്റാളേഷന്‍"

#~ msgid ""
#~ "Some changes mentioned in the previous"
#~ " section also imply changes in the"
#~ " support in the installer for "
#~ "automated installation using preconfiguration "
#~ "files.  This means that if you "
#~ "have existing preconfiguration files that "
#~ "worked with the |OLDRELEASENAME| installer,"
#~ " you cannot expect these to work "
#~ "with the new installer without "
#~ "modification."
#~ msgstr ""
#~ "മുന്നെ പ്രതിബാധിച്ചതു പോലെ ചില മാറ്റങ്ങള്‍ "
#~ "ഇന്‍സ്റ്റാളറിന്റെ സ്വയംനിയന്ത്രിത ഇന്‍സ്റ്റാളേഷന്‍ "
#~ "പിന്തുണയിലും മാറ്റങ്ങളുണ്ടാക്കിയിട്ടുണ്ട്. അതായത് "
#~ "|OLDRELEASENAME| ഇന്‍സ്റ്റാളറിനോടൊപ്പം "
#~ "പ്രവര്‍ത്തിച്ചിരുന്ന പ്രീകോണ്‍ഫിഗറേഷന്‍ ഫയലുകള്‍ "
#~ "നിങ്ങളുടെ പക്കലുണ്ടെങ്കില്‍ അവ ചില മാറ്റങ്ങള്‍"
#~ " വരുത്തിയെങ്കില്‍ മാത്രമേ പുതിയ "
#~ "ഇന്‍സ്റ്റോളറിനൊപ്പം പ്രവര്‍ത്തിയ്ക്കുകയുള്ളൂ."

#~ msgid ""
#~ "The <ulink url=\"&url-install-"
#~ "manual;\">Installation Guide</ulink> has an "
#~ "updated separate appendix with extensive "
#~ "documentation on using preconfiguration."
#~ msgstr ""
#~ "<ulink url=\"&url-install-manual;\">ഇന്‍സ്റ്റാളേഷന്‍"
#~ " സഹായി</ulink> യില്‍ പ്രീകോണ്‍ഫിഗറേഷന്‍ എങ്ങനെ"
#~ " ഉപയോഗിയ്ക്കണം എന്നത് സമ്പന്ധിച്ച ബൃഹത്തായ "
#~ "സഹായപ്രമാണത്തോടു കൂടിയ ഒരു പുതുക്കിയ അനുബന്ധം"
#~ " ലഭ്യമാണ്."

#~ msgid ""
#~ "The <ulink url=\"&url-cloud-team;\">cloud "
#~ "team</ulink> publishes Debian bullseye for "
#~ "several popular cloud computing services "
#~ "including:"
#~ msgstr ""

#~ msgid ""
#~ "Cloud images provide automation hooks "
#~ "via cloud-init and prioritize fast "
#~ "instance startup using specifically optimized"
#~ " kernel packages and grub configurations."
#~ "  Images supporting different architectures "
#~ "are provided where appropriate and the"
#~ " cloud team endeavors to support all"
#~ " features offered by the cloud "
#~ "service."
#~ msgstr ""

#~ msgid ""
#~ "More details are available at <ulink "
#~ "url=\"&url-cloud;\">cloud.debian.org</ulink> and "
#~ "<ulink url=\"&url-cloud-wiki;\">on the "
#~ "wiki</ulink>."
#~ msgstr ""

#~ msgid ""
#~ "Multi-architecture Debian bullseye container"
#~ " images are available on <ulink url"
#~ "=\"&url-docker-hub;\">Docker Hub</ulink>.  In "
#~ "addition to the standard images, a "
#~ "<quote>slim</quote> variant is available that"
#~ " reduces disk usage."
#~ msgstr ""

#~ msgid ""
#~ "Virtual machine images for the Hashicorp"
#~ " Vagrant VM manager are published to"
#~ " <ulink url=\"&url-vagrant-cloud;\">Vagrant "
#~ "Cloud</ulink>."
#~ msgstr ""

